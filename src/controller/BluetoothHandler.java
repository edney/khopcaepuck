package controller;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class BluetoothHandler {

	SerialPort serialPort;
	private static int BAUDRATE = 115200;
	private boolean flagConnected;

	public BluetoothHandler(String portName) {
		super();
		serialPort = new SerialPort(portName);
	}

	public boolean connect() {

		try {

			if (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				serialPort.closePort();
			}

			flagConnected = serialPort.openPort();// Open port
			while (!serialPort.isOpened()) {
				flagConnected = serialPort.openPort();// Open port
			}

			serialPort.setParams(BAUDRATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);// Set
																													// params
			return flagConnected;

		} catch (SerialPortException ex) {
			// System.out.println(ex);
			flagConnected = false;
			return flagConnected;
		}
	}

	public boolean disconnect() {

		boolean result = true;
		try {
			while (serialPort != null && serialPort.isOpened()) {
				serialPort.purgePort(1);
				serialPort.purgePort(2);
				result = serialPort.closePort();
			}

			flagConnected = false;

		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void sendBytes(byte[] msg) {

		try {
			serialPort.writeBytes(msg);
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public byte[] readBytes(int numberOfBytes) {
		byte[] realBytes = new byte[numberOfBytes];
		try {
			realBytes = this.serialPort.readBytes(numberOfBytes, 10000);
		} catch (SerialPortException | SerialPortTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return realBytes;
	}

	public SerialPort getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPort serialPort) {
		this.serialPort = serialPort;
	}

	public boolean isFlagConnected() {
		return flagConnected;
	}

	public void setFlagConnected(boolean flag) {
		this.flagConnected = flag;
	}

}