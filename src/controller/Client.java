package controller;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {

	Socket skt;
	static BufferedReader in;
	static PrintWriter out;

	public Client(String ip, int port) throws UnknownHostException, IOException {
		super();
		this.skt = new Socket(ip, port);
		out = new PrintWriter(skt.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
	}

	public static String request(String msg) throws IOException, InterruptedException {

		out.println(msg);
		Thread.sleep(10);
		while (!in.ready()) {
		}
		String response = in.readLine();
		return response;
	}

	public void disconnect() throws IOException {
		out.flush();
		out.close();
		in.close();
		skt.close();
	}

	public static void main(String args[]) throws InterruptedException {

		try {
			Client client = new Client("192.168.1.30", 1234);

			while (true) {
				Scanner scanner = new Scanner(System.in);
				int value = scanner.nextInt(); // para inteiros
				String msg = null;

				if (value != 0) {
					if (value == 1) {
						msg = "3040-02-00";
					}
					if (value == 2) {
						msg = "3040-03-00";
					}
					if (value == 3) {
						msg = "3118-02-00";
					}
					if (value == 4) {
						msg = "3118-03-00";
					}

					String result = client.request(msg);
					System.out.println(result);
				}

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
