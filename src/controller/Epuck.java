package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Epuck {

	private String portName;
	private String id;
	private ArrayList<Epuck> neighbors;
	private String k;
	private double positionX;
	private double positionY;
	private BluetoothHandler bluetooth;
	private Message message;
	private Fiducial fiducial;

	public Epuck(String portName) {
		super();
		this.portName = portName;
		neighbors = new ArrayList<Epuck>();
		this.message = new Message();
		Random rand = new Random();
		this.k = String.valueOf(rand.nextInt(9) + 1);

		if (portName.startsWith("remote")) {
			String[] parts = portName.split("-");
			this.id = "00" + parts[1];

		} else {
			bluetooth = new BluetoothHandler(portName);

		}
	}

	public static void main(String[] args) throws InterruptedException, IOException {

		Epuck epuck1 = new Epuck("COM3");
		Epuck epuck2 = new Epuck("COM8");
		Epuck epuck3 = new Epuck("COM10");
		Epuck epuck4 = new Epuck("COM12");
		Epuck epuck5 = new Epuck("COM14");
		Epuck epuck6 = new Epuck("COM16");
		epuck1.connect();
		epuck2.connect();
		epuck3.connect();
		epuck4.connect();
		epuck5.connect();
		epuck6.connect();

		while (true) {

			Thread.sleep(10);
			epuck1.getK();
			Thread.sleep(10);
			epuck2.getK();
			Thread.sleep(10);
			epuck3.getK();
			Thread.sleep(10);
			epuck4.getK();
			Thread.sleep(10);
			epuck5.getK();
			Thread.sleep(10);
			epuck6.getK();

		}

	}

	public void sendProximityRange(final String range) {
		new Thread() {
			@Override
			public void run() {

				// bluetooth.connect();
				byte[] msg = message.SEND_PROX;

				byte[] byteRange = range.getBytes();
				msg[5] = byteRange[0];
				msg[6] = byteRange[1];
				msg[7] = byteRange[2];

				bluetooth.sendBytes(msg);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}

				// bluetooth.disconnect();
				System.out.println("mandou o range:" + msg.toString());
			}
		}.start();
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getId() throws InterruptedException {

		if (this.id != null) {
			// System.out.println("pegou o id:" + id);
			return id;
		} else {

			// bluetooth.connect();
			byte[] msg = message.REQUEST_ID;
			bluetooth.sendBytes(msg);

			Thread.sleep(50);

			byte[] idReceived = bluetooth.readBytes(6);
			String str = new String(idReceived);
			id = str;
			// bluetooth.disconnect();
			// System.out.println("pegou o id:" + id);
		}

		return id;
	}

	public void sendStart() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-02-00";
			String response = Client.request(msg);
			System.out.println(response);
		} else {
			// bluetooth.connect();
			byte[] msg = message.SEND_START;
			bluetooth.sendBytes(msg);
			// bluetooth.disconnect();
			System.out.println("enviou o START");
		}
	}

	public void sendStop() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-03-00";
			String response = Client.request(msg);
			System.out.println(response);
		} else {
			// bluetooth.connect();
			byte[] msg = message.SEND_STOP;
			bluetooth.sendBytes(msg);
			// bluetooth.disconnect();
			System.out.println("enviou o STOP");
		}
	}

	public boolean connect() throws InterruptedException {

		if (portName.startsWith("remote")) {

			return true;

		} else {
			if (bluetooth.isFlagConnected()) {
				bluetooth.disconnect();
				Thread.sleep(1000);
				return bluetooth.connect();
			} else {
				return bluetooth.connect();
			}
		}

	}

	public void disconnect() {
		if (portName.startsWith("remote")) {
		} else {
			bluetooth.disconnect();
		}

	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Epuck> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(ArrayList<Epuck> neighbors) {
		this.neighbors = neighbors;
	}

	public void addNeighbor(Epuck neighbor) {
		this.neighbors.add(neighbor);
	}

	public void removeNeighbor(Epuck neighbor) {

		for (int i = 0; i < neighbors.size(); i++) {
			if (neighbors.get(i).getPortName().equals(neighbor.getPortName())) {
				this.neighbors.remove(i);
			}
		}
	}

	public String getK() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-04-";

			String quantityNeighbors = convertValueToFormatedString(getNeighbors().size());
			msg = msg.concat(quantityNeighbors + "&");

			for (int i = 0; i < getNeighbors().size(); i++) {
				String NeighborIDValue = convertValueToFormatedStringID(Integer.parseInt(getNeighbors().get(i).id));
				msg = msg.concat(NeighborIDValue + ",");
				String NeighborKValue = convertValueToFormatedString(Integer.parseInt(getNeighbors().get(i).k));
				msg = msg.concat(NeighborKValue + ",");

			}
			String kReceived = Client.request(msg);
			System.out.println(kReceived);
			k = kReceived;

			if (this.fiducial != null) {
				this.fiducial.setKhapa(Integer.parseInt(k));
				System.out.println("está entrando no setKhapa");
			}
			return k;
		} else {
			// bluetooth.connect();

			byte[] msg1 = message.REQUEST_K;

			String quantityNeighbors = convertValueToFormatedString(getNeighbors().size());
			byte[] qNeighbors = quantityNeighbors.getBytes();
			msg1[5] = qNeighbors[0];
			msg1[6] = qNeighbors[1];
			msg1[7] = qNeighbors[2];

			bluetooth.sendBytes(msg1);

			for (int i = 0; i < getNeighbors().size(); i++) {
				byte[] msg2 = message.SEND_NEIGHBORS;

				String NeighborIDValue = convertValueToFormatedStringID(Integer.parseInt(getNeighbors().get(i).id));
				byte[] NeighborID = NeighborIDValue.getBytes();
				msg2[4] = NeighborID[0];
				msg2[5] = NeighborID[1];
				msg2[6] = NeighborID[2];
				msg2[7] = NeighborID[3];

				bluetooth.sendBytes(msg2);

				byte[] msg3 = message.SEND_NEIGHBORS;

				String NeighborKValue = convertValueToFormatedString(Integer.parseInt(getNeighbors().get(i).k));
				byte[] NeighborK = NeighborKValue.getBytes();
				msg3[5] = NeighborK[0];
				msg3[6] = NeighborK[1];
				msg3[7] = NeighborK[2];

				bluetooth.sendBytes(msg3);
			}
			byte[] kReceived = bluetooth.readBytes(6);
			String str = new String(kReceived).substring(3, 6);
			k = str;

			// bluetooth.disconnect();

			if (this.fiducial != null) {

				try {
					this.fiducial.setKhapa(Integer.parseInt(k));
				} catch (NumberFormatException e) {
					System.out.println("formou um numero errado, provavelmente string vazia");
				}

			}

			System.out.println(k);
			return k;
		}
	}

	public void setK(String k) {
		this.k = k;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public void setPositionXY(double x, double y) {
		this.positionX = x;
		this.positionY = y;
	}

	public void addNeighbors(Epuck epuck) {
		this.neighbors.add(epuck);
	}

	public void removeNeighbors(Epuck epuck) throws InterruptedException {
		for (int i = 0; i < this.neighbors.size(); i++) {
			if (this.neighbors.get(i).getId().equals(epuck.getId())) {
				this.neighbors.remove(i);
			}
		}
	}

	public static String convertValueToFormatedString(int value) {

		StringBuilder resultValue = new StringBuilder("000");
		String converted = String.valueOf(value);

		for (int i = 1; i <= converted.length(); i++) {
			resultValue.setCharAt(resultValue.length() - i, converted.charAt(converted.length() - i));
		}
		return resultValue.toString();
	}

	public static String convertValueToFormatedStringID(int value) {

		StringBuilder resultValue = new StringBuilder("0000");
		String converted = String.valueOf(value);

		for (int i = 1; i <= converted.length(); i++) {
			resultValue.setCharAt(resultValue.length() - i, converted.charAt(converted.length() - i));
		}
		return resultValue.toString();
	}

	public Fiducial getFiducial() {
		return fiducial;
	}

	public void setFiducial(Fiducial fiducial) {
		this.fiducial = fiducial;
	}

	public BluetoothHandler getBluetooth() {
		return bluetooth;
	}

}