package controller;

import java.nio.charset.StandardCharsets;

public class Message {

	public final static int BT_MSG_LENGTH = 10;

	public byte[] REQUEST_ID = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_K = new byte[BT_MSG_LENGTH];
	public byte[] SEND_START = new byte[BT_MSG_LENGTH];
	public byte[] SEND_STOP = new byte[BT_MSG_LENGTH];
	public byte[] SEND_PROX = new byte[BT_MSG_LENGTH];
	public byte[] SEND_NEIGHBORS = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_MIN_NEIGHBORS = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_SEGUNDO = new byte[BT_MSG_LENGTH];

	public Message() {

		REQUEST_ID = mountMessage("000001");
		REQUEST_K = mountMessage("500000");
		REQUEST_MIN_NEIGHBORS = mountMessage("700000");
		REQUEST_SEGUNDO = mountMessage("800000");
		SEND_START = mountMessage("100001");
		SEND_STOP = mountMessage("100002");
		SEND_PROX = mountMessage("300000");
		SEND_NEIGHBORS = mountMessage("600000");
	}

	public static void main(String[] args) {
		byte[] test = mountMessage("000001");
		System.out.println(test.toString());
	}

	private static byte[] mountMessage(String msg) {

		byte[] message = msg.getBytes(StandardCharsets.US_ASCII);
		byte[] buffer = new byte[BT_MSG_LENGTH];

		buffer[0] = (byte) 0x16;
		buffer[1] = (byte) 0x02;

		for (int i = 2; i < BT_MSG_LENGTH - 2; i++)
			buffer[i] = message[i - 2];

		buffer[BT_MSG_LENGTH - 2] = (byte) 0x03;
		buffer[BT_MSG_LENGTH - 1] = (byte) 0x17;

		return buffer;
	}
}
