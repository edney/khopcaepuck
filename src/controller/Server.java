package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectableChannel;
import java.util.List;

class Server {

	private List<Epuck> epuckList;
	private int port;
	BufferedReader in;
	PrintWriter out;

	public Server(List<Epuck> epuckList, int port) throws IOException {
		super();
		this.epuckList = epuckList;
		this.port = port;
		ServerSocket srvr = new ServerSocket(1234);
		Socket skt = srvr.accept();
		System.out.print("Server has connected!\n");
		out = new PrintWriter(skt.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
	}

	public void listen() throws InterruptedException {

		String response;

		while (true) {
			try {
				while (!in.ready()) {
				}
				String line = in.readLine();
				System.out.println(line);

				response = processRequest(line);

				out.println(response);

			} catch (IOException e) {
				System.out.print("Whoops! It didn't work!\n");
			}

		}
	}

	private String processRequest(String line) throws InterruptedException, IOException {

		String[] parts = line.split("-");
		String epuckId = parts[0];
		String opCode = parts[1];
		String param = parts[2];

		for (int i = 0; i < epuckList.size(); i++) {

			if (epuckList.get(i).getId().equals(epuckId)) {

				switch (opCode) {
				case "01":
					return epuckList.get(i).getK();
				default:
					break;
				}
			}
		}
		return null;
	}

	public static void main(String args[]) {
		String data = "Toobie ornaught toobie";
		try {
			ServerSocket srvr = new ServerSocket(1234);
			Socket skt = srvr.accept();
			System.out.print("Server has connected!\n");
			PrintWriter out = new PrintWriter(skt.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			System.out.print("Sending string: '" + data + "'\n");

			while (true) {
				while (!in.ready()) {
				}
				String line = in.readLine();
				System.out.println(line);
				out.println("Essa é a resposta do servidor\n");
			}
		} catch (Exception e) {
			System.out.print("Whoops! It didn't work!\n");
		}
	}
}
