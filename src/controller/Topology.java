package controller;

import georegression.struct.point.Point2D_I32;
import georegression.struct.shapes.Quadrilateral_F64;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Robot;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import view.CamWindow;
import view.DrawFiducial;
import view.RobotWindow;

import boofcv.alg.filter.binary.GThresholdImageOps;

public class Topology {

	private List<Epuck> epuckList;
	private Map<Integer, String> mapFiducial;

	public Topology() {
		super();
		epuckList = new ArrayList<>();
		mapFiducial = new HashMap<Integer, String>();

	}

	public Topology(List<Epuck> epuckList) {
		super();
		this.epuckList = epuckList;
	}

	public List<Epuck> getEpuckList() {
		return epuckList;
	}

	public void setEpuckList(List<Epuck> epuckList) {
		this.epuckList = epuckList;
	}

	public void addEpuck(Epuck epuck) {
		this.epuckList.add(epuck);
	}

	public void removeEpuck(Epuck epuck) {
		this.epuckList.remove(epuck);
	}

	public Epuck getEpuckById(String id) throws InterruptedException {

		for (int i = 0; i < this.epuckList.size(); i++) {
			if (getEpuckList().get(i).getId().equals(id)) {
				return this.epuckList.get(i);
			}
		}
		return null;
	}

	public void update(ArrayList<Fiducial> fiducialList, int radiationRadius) throws InterruptedException, IOException {

		for (int i = 0; i < getEpuckList().size(); i++) {

			if (getEpuckList().get(i).getFiducial().isActive()) {

				for (int j = 0; j < getEpuckList().size(); j++) {

					if (getEpuckList().get(j).getFiducial().isActive()) {

						Point2D_I32 first = getEpuckList().get(i).getFiducial().getCenter();
						Point2D_I32 second = getEpuckList().get(j).getFiducial().getCenter();

						double distance = DrawFiducial.getEuclidianDistance(first, second);

						if ((distance <= radiationRadius) && (distance >= 5.0)) {

							if (!getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).addNeighbor(getEpuckList().get(j));
							}
						} else {

							if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
							}
						}
					} else {

						if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

							getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
						}
					}

				}
				String khapa = getEpuckList().get(i).getK();

				System.out.println("setou weight com " + i + "-" + khapa);

				RobotWindow.weight.set(i, Integer.parseInt(khapa));
			}
		}

	}

	public Map<Integer, String> getMapFiducial() {
		return mapFiducial;
	}

	public void setMapFiducial(Map<Integer, String> mapFiducial) {
		this.mapFiducial = mapFiducial;
	}

	public void setFiducial(Fiducial fiducial) throws InterruptedException {

		for (int i = 0; i < this.epuckList.size(); i++) {
			if (getEpuckList().get(i).getId().equals("00" + this.mapFiducial.get(fiducial.getId()))) {
				getEpuckList().get(i).setFiducial(fiducial);
			}
		}
	}

	public void removeFiducial(int index) throws InterruptedException {

		getEpuckList().get(index).setFiducial(null);
	}

	public void removeFiducials() {
		for (int i = 0; i < this.epuckList.size(); i++) {

			epuckList.get(i).setFiducial(null);
		}
	}
}
