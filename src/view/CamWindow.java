package view;

import georegression.metric.UtilAngle;
import georegression.struct.se.Se3_F64;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import boofcv.abst.fiducial.SquareImage_to_FiducialDetector;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.fiducial.ConfigFiducialImage;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.calib.IntrinsicParameters;
import boofcv.struct.image.ImageFloat32;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.openimaj.OpenImajDriver;

import controller.Fiducial;

public class CamWindow {

	static {
		Webcam.setDriver(new OpenImajDriver());
	}

	Webcam webcam;
	int cameraId;
	ImageFloat32 gray;
	SquareImage_to_FiducialDetector<ImageFloat32> detector;
	IntrinsicParameters param;
	Dimension[] resolution;
	private double fiducialMinContourFraction;
	private int fiducialQuantity;
	private ArrayList<Fiducial> fiducialList;
	private ArrayList<Integer> flagUpdated;
	private int radiationRadius;

	public CamWindow(int cameraId, Dimension dimension, int fiducialQuantity, int radiation) {

		super();
		this.cameraId = cameraId;
		this.webcam = Webcam.getWebcams().get(cameraId);
		this.resolution = new Dimension[] { dimension };
		this.webcam.setCustomViewSizes(this.resolution);
		this.webcam.setViewSize(this.resolution[0]);
		this.fiducialMinContourFraction = 0.0005;
		this.fiducialQuantity = fiducialQuantity;
		this.fiducialList = new ArrayList<Fiducial>(fiducialQuantity);
		this.flagUpdated = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));
		this.radiationRadius = radiation;
	}

	public void init() {

		this.webcam.open();
		this.param = new IntrinsicParameters();
		Dimension d = webcam.getDevice().getResolution();
		this.param.width = d.width;
		this.param.height = d.height;
		this.param.cx = d.width / 2;
		this.param.cy = d.height / 2;
		this.param.fx = this.param.cx / Math.tan(UtilAngle.degreeToRadian(30));
		this.param.fy = this.param.cx / Math.tan(UtilAngle.degreeToRadian(30));
		this.param.flipY = false;

		ConfigFiducialImage config = new ConfigFiducialImage(10.0);
		config.setMinContourFraction(fiducialMinContourFraction);
		detector = FactoryFiducial.squareImageRobust(config, 6, ImageFloat32.class);
		List<ImageFloat32> images = new ArrayList<>();

		for (int i = 0; i < this.fiducialQuantity; i++) {
			images.add(UtilImageIO.loadImage("resource/" + i + ".png", ImageFloat32.class));
			detector.addTarget(images.get(i), 125);
			fiducialList.add(new Fiducial(0, 0, 0));

		}

		detector.setIntrinsic(this.param);
		gray = new ImageFloat32((int) this.resolution[0].getWidth(), (int) this.resolution[0].getHeight());
	}

	public void process() {

		ImagePanel gui = new ImagePanel((int) this.resolution[0].getWidth(), (int) this.resolution[0].getHeight());
		ShowImages.showWindow(gui, "Augmented Epuck");

		while (true) {

			BufferedImage frame = webcam.getImage();
			ConvertBufferedImage.convertFrom(frame, gray);
			detector.detect(gray);
			Graphics2D g2 = frame.createGraphics();
			Se3_F64 targetToSensor = new Se3_F64();
			flagUpdated = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));

			DrawFiducial.drawRadiation(fiducialList, g2, radiationRadius);

			for (int i = 0; i < detector.totalFound(); i++) {

				int index = detector.getId(i);
				detector.getFiducialToWorld(i, targetToSensor);

				if ((RobotWindow.updated) && (RobotWindow.weight.get(index).equals(0))) {
					DrawFiducial.drawSquare(targetToSensor, this.param, 10.0, g2, Color.RED);
				} else {
					DrawFiducial.drawSquare(targetToSensor, this.param, 10.0, g2, Color.GREEN);
				}

				Fiducial fiducial = new Fiducial(targetToSensor.getX(), targetToSensor.getY(), Math.toDegrees(
						-Math.atan2(targetToSensor.getRotation().get(2), targetToSensor.getRotation().get(5))));
				fiducial.setCenter(targetToSensor, this.param, 10.0);
				fiducial.setId(index);
				fiducial.setActive(true);
				fiducialList.set(index, fiducial);
				flagUpdated.set(index, 1);

				DrawFiducial.drawRange(fiducialList.get(index), g2, radiationRadius);
				// fiducialList.get(index).setKhapa(8);

				if (RobotWindow.updated) {

					System.out.println("valor que veio do robotwindow " + index + "-" + RobotWindow.weight.get(index));
					DrawFiducial.drawKhapa(fiducialList.get(index), RobotWindow.weight.get(index), g2);

				}

			}

			for (int i = 0; i < fiducialList.size(); i++) {
				if (flagUpdated.get(i) != 1) {
					fiducialList.get(i).setActive(false);
				}
			}

			gui.setBufferedImageSafe(frame);
			gui.repaint();
		}
	}

	public double getFiducialMinContourFraction() {
		return fiducialMinContourFraction;
	}

	public void setFiducialMinContourFraction(double fiducialMinContourFraction) {
		this.fiducialMinContourFraction = fiducialMinContourFraction;
	}

	public ArrayList<Fiducial> getFiducialList() {
		return fiducialList;
	}

	public void setFiducialList(ArrayList<Fiducial> fiducialList) {
		this.fiducialList = fiducialList;
	}

	public int getRadiationRadius() {
		return radiationRadius;
	}

	public void setRadiationRadius(int radiationRadius) {
		this.radiationRadius = radiationRadius;
	}
}
