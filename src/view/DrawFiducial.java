package view;

import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.struct.point.Point3D_F64;
import georegression.struct.se.Se3_F64;
import georegression.struct.shapes.Quadrilateral_F64;
import georegression.transform.se.SePointOps_F64;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import controller.Fiducial;

import boofcv.alg.geo.PerspectiveOps;
import boofcv.struct.calib.IntrinsicParameters;

public class DrawFiducial {
	
		
	public static void drawSquare( Se3_F64 targetToCamera , IntrinsicParameters intrinsic , double width , Graphics2D g2, Color color ){
		
		double r = width/2.0;
		Point3D_F64 corners[] = new Point3D_F64[9];
		corners[0] = new Point3D_F64(-r,-r,0);
		corners[1] = new Point3D_F64( r,-r,0);
		corners[2] = new Point3D_F64( r, r,0);
		corners[3] = new Point3D_F64(-r, r,0);
		corners[4] = new Point3D_F64(-r,-r,r);
		corners[5] = new Point3D_F64( r,-r,r);
		corners[6] = new Point3D_F64( r, r,r);
		corners[7] = new Point3D_F64(-r, r,r);
		corners[8] = new Point3D_F64(0, 0,0);
		
		
		Point2D_I32 pixel[] = new Point2D_I32[9];
		Point2D_F64 p = new Point2D_F64();
		
		for (int i = 0; i < 9; i++) {
			Point3D_F64 c = corners[i];
			SePointOps_F64.transform(targetToCamera,c,c);
			PerspectiveOps.convertNormToPixel(intrinsic,c.x/c.z,c.y/c.z,p);
			pixel[i] = new Point2D_I32((int)(p.x+0.5),(int)(p.y+0.5));
		}
		
		g2.setStroke(new BasicStroke(4));
		g2.setColor(color);
		g2.drawLine(pixel[0].x,pixel[0].y,pixel[1].x,pixel[1].y);
		g2.drawLine(pixel[1].x,pixel[1].y,pixel[2].x,pixel[2].y);
		g2.drawLine(pixel[2].x,pixel[2].y,pixel[3].x,pixel[3].y);
		g2.drawLine(pixel[3].x,pixel[3].y,pixel[0].x,pixel[0].y);
		
		
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f));
		int[] xPoints = new int[4]; 
		int[] yPoints = new int[4];
		
		xPoints[0] = pixel[0].x;
		xPoints[1] = pixel[1].x;
		xPoints[2] = pixel[2].x;
		xPoints[3] = pixel[3].x;
		
		yPoints[0] = pixel[0].y;
		yPoints[1] = pixel[1].y;
		yPoints[2] = pixel[2].y;
		yPoints[3] = pixel[3].y;
		
		g2.fillPolygon(xPoints, yPoints, 4);
		
		
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
		
		
	}

	public static void drawRadiation(ArrayList<Fiducial> fiducialList, Graphics2D g2, int radiationRadius) {
		
		g2.setColor(Color.yellow);
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));	
		final float dash1[] = {15.0f, 20.0f};
	    final BasicStroke dashed = new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
	    g2.setStroke(dashed);
	    
		for (int i = 0; i < fiducialList.size(); i++) {
			
			Fiducial current = fiducialList.get(i);
			
			if(current != null){
				
				for (int j = 0; j < fiducialList.size(); j++) {
					
					if(fiducialList.get(j) != null){
						if(current.isActive() && fiducialList.get(j).isActive()){
							
							double  distance = getEuclidianDistance(current.getCenter(),fiducialList.get(j).getCenter()); 
							
							if(distance <= radiationRadius && distance != 0){
								
								g2.drawLine(current.getCenter().x, current.getCenter().y, fiducialList.get(j).getCenter().x, fiducialList.get(j).getCenter().y);
							}
							
						}
					}
				}
			}
		}
	}
	
	public static double getEuclidianDistance(Point2D_I32 first, Point2D_I32 second){
		
		double x1 = first.getX();
		double y1 = first.getY();
		
		double x2 = second.getX();
		double y2 = second.getY();
				
		double  xDiff = x1-x2;
	    double  xSqr  = Math.pow(xDiff, 2);
		double yDiff = y1-y2;
		double ySqr = Math.pow(yDiff, 2);
		double output   = Math.sqrt(xSqr + ySqr);
		
		
		return output;  
	}

	public static void drawRange(Fiducial fiducial, Graphics2D g2, int radiationRadius) {
		
		g2.setColor(Color.yellow);
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f));
		
		g2.fillOval((int)fiducial.getCenter().x - radiationRadius/2, (int)fiducial.getCenter().y - radiationRadius/2, radiationRadius, radiationRadius);
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));	
	}

	public static void drawKhapa(Fiducial fiducial, int khapa, Graphics2D g2) {
		
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
		Font bigFont = new Font("Arial", Font.BOLD, 24);
		Font smallFont = new Font("Arial", Font.BOLD, 12);
		g2.setFont(bigFont);
		
		if(fiducial != null){
			g2.setColor(Color.WHITE);
			g2.fillOval(fiducial.getCenter().x - 15, fiducial.getCenter().y - 15, 30, 30);
			g2.setColor(Color.BLACK);			
			g2.drawString(String.valueOf(khapa), fiducial.getCenter().x - 6, fiducial.getCenter().y + 7);
		}
	}
}
