package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class EpuckNetGUI extends JFrame {

	public EpuckNetGUI() throws UnknownHostException, IOException, ParseException {

		initUI();
	}

	public void initUI() throws UnknownHostException, IOException, ParseException {

		JToolBar toolbar = new JToolBar();

		final CamWindow camWindow = new CamWindow(0, new Dimension(1280, 720), 10, 200);

		final RobotWindow robotWindow = new RobotWindow(camWindow, 10);
		robotWindow.setSize(640, 640);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		ImageIcon camIcon = new ImageIcon("resource/cam.png");
		ImageIcon robotIcon = new ImageIcon("resource/robot.png");
		ImageIcon exitIcon = new ImageIcon("resource/exit.png");

		JButton camButton = new JButton(camIcon);
		JButton robotButton = new JButton(robotIcon);
		JButton exitButton = new JButton(exitIcon);

		toolbar.add(camButton);
		toolbar.add(robotButton);
		toolbar.add(exitButton);
		toolbar.setAlignmentX(0);

		camButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {

				new Thread() {
					@Override
					public void run() {
						camWindow.init();
						camWindow.process();
					}
				}.start();
			}
		});

		robotButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					@Override
					public void run() {
						robotWindow.setVisible(true);
						try {
							robotWindow.process();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();
			}
		});

		panel.add(toolbar);
		add(panel, BorderLayout.NORTH);
		setTitle("EpuckNet");
		setSize(640, 100);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				EpuckNetGUI ex = null;
				try {
					ex = new EpuckNetGUI();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ex.setVisible(true);
			}
		});
	}
}