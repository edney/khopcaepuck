package view;

import java.awt.Dimension;

public class Main {

	public static void main(String[] args) {

		CamWindow fiducial = new CamWindow(1, new Dimension(1280, 960), 2, 400);
		fiducial.init();
		fiducial.process();
	}
}
