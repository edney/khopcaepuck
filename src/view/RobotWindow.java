package view;

import georegression.struct.shapes.Quadrilateral_F64;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Client;
import controller.Epuck;
import controller.Fiducial;
import controller.Topology;

public class RobotWindow extends JFrame {
	Topology topology;
	public static boolean runningAlgorithm;
	public static boolean updatedTopology;
	public static boolean clientConnected;
	public int radiationRadius;
	public List<Quadrilateral_F64> target;
	CamWindow camWindow;
	public static boolean updated;
	public static ArrayList<Integer>weight;
	Client client;
	JPanel panel1;
	JPanel panel2;
	JPanel panel3;
	JPanel panelListEpucks;
	
		
	public RobotWindow(CamWindow camWindow, int fiducialQuantity) throws UnknownHostException, IOException{
		this.camWindow = camWindow;
		this.radiationRadius = camWindow.getRadiationRadius();
		target = new ArrayList<Quadrilateral_F64>();
		topology = new Topology();
		initUI();
		runningAlgorithm = false;
		updatedTopology = false;
		weight = new ArrayList<Integer>(Collections.nCopies(fiducialQuantity, 0));
		updated = false;
		clientConnected = false;
	}

	private void initUI() {
		
		final Container screen = getContentPane();
		screen.setLayout(new BoxLayout(screen,BoxLayout.Y_AXIS));
		 
		JLabel comPortEpuckLabel = new JLabel("Porta:");
		final JTextField comPortEpuck = new JTextField(5);
		JButton addEpuck = new JButton("Add Epuck");
		
		JLabel rangeProxLabel = new JLabel("Range Proximity:");
		final JTextField rangeProx = new JTextField(5);
		JButton sendRange = new JButton("Send Range");
				
		DefaultListModel model = new DefaultListModel();
		final JList listComPort = new JList(model);
			    
		JButton start = new JButton("Start");
	    JButton stop = new JButton("Stop");
	    JButton getID = new JButton("get ID");
	    
	    JLabel algorithmLabel = new JLabel("Set Algorithm:");
	    final JButton khopca = new JButton("Start KHOPCA");
	    
	    JLabel ipLabel = new JLabel("Ip:");
	    final JTextField ipClient = new JTextField(10);
	    JLabel portLabel = new JLabel("Porta");
	    final JTextField portClient = new JTextField(5);
	    final JButton connectClient = new JButton("conectar");
	    final JButton testClient = new JButton("testar");
	    
	    
	    topology.addEpuck(new Epuck("COM12"));
	    topology.addEpuck(new Epuck("COM16"));
	    topology.addEpuck(new Epuck("COM10"));
	    topology.addEpuck(new Epuck("COM8"));
	    topology.addEpuck(new Epuck("COM3"));
	    topology.addEpuck(new Epuck("COM14"));
	    
	    topology.addEpuck(new Epuck("remote-3054"));
	    topology.addEpuck(new Epuck("remote-2816"));
	    topology.addEpuck(new Epuck("remote-2926"));
	    topology.addEpuck(new Epuck("remote-2979"));
	    
	    Map<Integer, String> map = new HashMap<Integer, String>();
	    
	    map.put(0, "2948");
	    map.put(1, "3167");
	    map.put(2, "3080");
	    map.put(3, "3040");
	    map.put(4, "3118");
	    map.put(5, "3177");
	    
	    map.put(6, "3054");
	    map.put(7, "2816");
	    map.put(8, "2926");
	    map.put(9, "2979");
	    
	    
	    topology.setMapFiducial(map);
	    
	    panel1 = new JPanel();
	    panel2 = new JPanel();
	    panel3 = new JPanel();
	    panelListEpucks = new JPanel();
	    
	    panel1.setLayout(new FlowLayout());
	    panel1.setBorder(BorderFactory.createTitledBorder("Conectar Cliente"));
	    panel1.add(ipLabel);
	    panel1.add(ipClient);
	    panel1.add(portLabel);
	    panel1.add(portClient);
	    panel1.add(connectClient);
	    panel1.add(testClient);
	        
	    panel2.setLayout(new FlowLayout());
	    panel2.setBorder(BorderFactory.createTitledBorder("Comandos"));
	    panel2.add(start);
	    panel2.add(stop);
	    panel2.add(getID);
	    panel2.add(algorithmLabel);
	    panel2.add(khopca);
	    panel2.add(rangeProxLabel);
	    panel2.add(rangeProx);
	    panel2.add(sendRange);
	        
	    panel3.setLayout(new FlowLayout());
	    panel3.setBorder(BorderFactory.createTitledBorder("Gerenciar Epucks"));
	   
	    panelListEpucks.setLayout(new BoxLayout(panelListEpucks, BoxLayout.Y_AXIS));
	    
	    for (int j = 0; j < topology.getEpuckList().size(); j++) {
			JPanel itemPanel = new JPanel();
			final JButton button = new JButton("conectar");
			itemPanel.setLayout(new FlowLayout());
			itemPanel.add(new JLabel(map.get(j)));		
	    	itemPanel.add(button);
	    	panelListEpucks.add(itemPanel);
	    	
	    	button.addActionListener(new ActionListener() {
	            private int innerValue;
	            private boolean connected;
	    		public void actionPerformed(ActionEvent event) {
	            	try {
					
	            		if (!topology.getEpuckList().get(innerValue).getPortName().startsWith("remote")) {
						
	            			if(topology.getEpuckList().get(innerValue).getBluetooth().isFlagConnected()){
		            			
		            			topology.getEpuckList().get(innerValue).disconnect();
		            			button.setText("Conectar");
		            		}
		            		else{
		            			connected = topology.getEpuckList().get(innerValue).connect();
		    					
								if(!connected){
									System.out.println("não conseguiu connectar");
								}
								else{
									System.out.println("conectou");
									button.setText("Desconectar");
								}
		            		}
	            			
						}
	            		else{
	            			connected = topology.getEpuckList().get(innerValue).connect();
	    					
							if(!connected){
								System.out.println("não conseguiu connectar");
							}
							else{
								System.out.println("conectou");
								button.setText("Desconectar");
							}
	            		}
					
					} catch (InterruptedException e) {
						System.out.println("falha no metodo de conexão conectar");
						//e.printStackTrace();
					}
	            	
	              
	            }
	            private ActionListener init(int var){
	            	innerValue = var;
	            	return this;
	            }
	            
	            
	        }.init(j));
	    	
	    	
		}
	    panel3.add(panelListEpucks);
		
	    screen.add(panel1);
	    screen.add(panel2);
	    screen.add(panel3);
	   
	    	    
	    addEpuck.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                topology.addEpuck(new Epuck(comPortEpuck.getText()));
            	
                if (!comPortEpuck.getText().isEmpty() && !comPortEpuck.getText().isEmpty()) {
                	
                	DefaultListModel model = (DefaultListModel) listComPort.getModel();
                	model.add(model.getSize(), comPortEpuck.getText());
                	comPortEpuck.setText("");
				}
            }
        });
		
	    start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendStart();
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            }
        });
	    
	    stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendStop();
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            }
        });
	    	    
	    getID.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	new Thread() {
					@Override
					public void run() {
						
						for(int i= 0; i< topology.getEpuckList().size(); i++){
		            		try {
								topology.getEpuckList().get(i).getId();
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
		            	}
					}
            	}.start();
            }
        });
	    
	    sendRange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	for(int i= 0; i< topology.getEpuckList().size(); i++){
            		try {
						topology.getEpuckList().get(i).sendProximityRange(rangeProx.getText());
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
            	}
            }
        });
	    
	    khopca.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	
            	if(!runningAlgorithm){
            		runningAlgorithm = true;
					khopca.setText("Stop  KHOPCA");
					
					new Thread() {
						@Override
						public void run() {
					                        		
							try {
								
								for(int i= 0; i< topology.getEpuckList().size(); i++){
				            		try {
										topology.getEpuckList().get(i).getId();
										topology.getEpuckList().get(i).sendStart();
										Thread.sleep(100);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
				            	}
								runKhopca();
							} catch (InterruptedException e) {
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	
					}.start();
            	}else{
            		runningAlgorithm = false;
            		khopca.setText("Start KHOPCA");
            	}
            }
        });
	    
	    connectClient.addActionListener(new ActionListener() {

			@Override
            public void actionPerformed(ActionEvent event) {
            	if (!clientConnected) {
            		try {
						client = new Client(ipClient.getText(), Integer.parseInt(portClient.getText()));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		clientConnected = true;
            		connectClient.setText("desconectar");
				}
            	else{
            		try {
            			client.request("disconnect");
						client.disconnect();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		clientConnected = false;
            		connectClient.setText("conectar");
            	}
            }
        });
	    
	    testClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
            	if (clientConnected) {
					try {
						String testeResult = client.request("teste");
						System.out.println(testeResult);
					} catch (IOException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
            }
        });
	   
	    
	    
	}
	
	public void process() throws InterruptedException{
		
		for (int i = 0; i < topology.getEpuckList().size(); i++) {
			topology.getEpuckList().get(i).setFiducial(new Fiducial(0,0, 0));
		}
		
		
		while (true) {
			
		
			
			for (int i = 0; i < camWindow.getFiducialList().size(); i++) {
				
				if(camWindow.getFiducialList() != null){
				
					if(camWindow.getFiducialList().get(i) != null){
						
						if(camWindow.getFiducialList().get(i).isActive() ){
							topology.setFiducial(camWindow.getFiducialList().get(i));
							
						}
					}
				}
			}	
		}
	}
	
	
	public Topology getTopology() {
		return topology;
	}

	public void setTopology(Topology topology) {
		this.topology = topology;
	}
			
	public int getRadiationRadius() {
		return radiationRadius;
	}

	public void setRadiationRadius(int radiationRadius) {
		this.radiationRadius = radiationRadius;
	}
	
	public static boolean runningAlgorithm(){
		return runningAlgorithm;
	}
	
	public static boolean updatedTopology(){
		return updatedTopology;
	}
	
	public List<Quadrilateral_F64> getTarget() {
		return target;
	}

	public void setTarget(List<Quadrilateral_F64> target) {
		this.target = target;
	}

	protected void runKhopca() throws InterruptedException, IOException { 
		
				
		while(runningAlgorithm){
			
			topology.update(camWindow.getFiducialList(),radiationRadius);
			
			updated = true;
		}
	}
}